#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pip, os
from subprocess import call
from pip._internal import main

SUBMODULES_FOLDERS = [
    'mccdaqr',
    'toolsr',
    'sound-calibration-plugin',
    'trend-2fc-session-plugin',
    'water-calibration-plugin',
]

def install():
    for submodule in SUBMODULES_FOLDERS:
        main(['install', '-e', os.path.join(submodule,'.')])

def check_submodules():
    for submodule in SUBMODULES_FOLDERS:
        if not os.path.exists(os.path.join(submodule,'setup.py')):
            call(["git", "submodule", "update", "--init", "--recursive"])
            break

if __name__=='__main__':
    check_submodules()
    install()
    print("-----> Installation completed.")
